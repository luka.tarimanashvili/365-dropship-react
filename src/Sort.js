import { useDispatch } from "react-redux";
import { sortProduct } from "./components/SortFunction";

export const Sort = () => {
  const dispatch = useDispatch();
  const sortProd = (e) => {
    dispatch(sortProduct(e.target.value));
  };

  return (
    <div className="sort">
      <label htmlFor="sort-select" className="sort-label sort-text">
        <div className="sort-filter">
          <div className="sort-filter-item sort-filter-1" />
          <div className="sort-filter-item sort-filter-2" />
          <div className="sort-filter-item sort-filter-3" />
        </div>
        <span>Sort By:</span>
        <select
          id="sort-select"
          className="sort-text sort-select"
          onChange={sortProd}
          defaultValue=""
        >
          <option value="newArvl">New Arrivals</option>
          <option value="az">Title: Ascending</option>
          <option value="za">Title: Descending</option>
          <option value="asc">Price: Low To High</option>
          <option value="desc">Price: High To Low</option>
        </select>
        <div className="sort-text sort-carets">
          <span>&#9650;</span>
          <span>&#9660;</span>
        </div>
      </label>
    </div>
  );
};
