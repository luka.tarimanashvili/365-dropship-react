import { sortProducts } from "../../components/SortFunction";

const initialState = {
  catalog: [],
  cart: [],
  modalOpen: false,
  sortedProducts: [],
};

export default function ProductsReducer(state = initialState, action) {
  switch (action.type) {
    case "Products_Arrived":
      return { ...state, catalog: action.payload };
    case "Cart_Arrived":
      return { ...state, cart: action.payload };
    case "SORT_PRODUCTS":
      return {
        ...state,
        catalog: sortProducts(action.payload.sort, [...state.catalog]),
        sortedProducts: sortProducts(action.payload.sort, [
          ...state.sortedProducts,
        ]),
      };
    default:
      return state;
  }
}
