import axios from "axios";

const Api = (query) => {
  const SERVER_ADDRESS = "http://18.185.148.165:3000";
  return axios.create({
    baseURL: `${SERVER_ADDRESS}/${query}`,
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });
};

export default Api;
