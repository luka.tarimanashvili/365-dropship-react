import { ItemCard } from "./components/ItemCard";
import { Grid } from "@material-ui/core";
import { useSelector } from "react-redux";

export const ItemsList = ({ toggleSelect, cardImage, inventory }) => {
  const store = useSelector((state) => state.catalog);
  const cart = useSelector((state) => state.cart);

  console.log("cart", cart);
  console.log("hgjhvv", store);
  return inventory ? (
    <Grid container spacing={0}>
      {store.map((item) => (
        <Grid xs={12} sm={6} md={4} lg={3}>
          <ItemCard
            key={item.id}
            item={item}
            toggleSelect={toggleSelect}
            inventory={inventory}
          />
        </Grid>
      ))}
    </Grid>
  ) : (
    <Grid container spacing={0}>
      {cart.map((item) => (
        <Grid xs={12} sm={6} md={4} lg={3}>
          <ItemCard
            key={item.id}
            item={item}
            toggleSelect={toggleSelect}
            cardImage={cardImage}
          />
        </Grid>
      ))}
    </Grid>
  );
};
