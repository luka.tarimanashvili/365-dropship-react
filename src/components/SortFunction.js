export const sortProducts = (sort, data) => {
  sort === "newArvl" && data.sort((a, b) => a.id - b.id);
  sort === "asc" && data.sort((a, b) => a.price - b.price);
  sort === "desc" && data.sort((a, b) => b.price - a.price);
  sort === "az" && data.sort((a, b) => (a.title > b.title ? 1 : -1));
  sort === "za" && data.sort((a, b) => (a.title > b.title ? -1 : 1));

  return data;
};

export const sortProduct = (sort) => {
  return {
    type: "SORT_PRODUCTS",
    payload: {
      sort,
    },
  };
};
