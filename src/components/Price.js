const Price = ({ item }) => {
  return (
    <div className="price">
      <div className="price-container">
        <div className="price-num">{item.price}$</div>
        <div className="price-title">RPR</div>
      </div>
      <div className="price-container price-mid">
        <div className="price-num">{item.price}$</div>
        <div className="price-title">COST</div>
      </div>
      <div className="price-container">
        <div className="price-num price-num-cyan">{item.price}$</div>
        <div className="price-title">PROFIT</div>
      </div>
    </div>
  );
};

export default Price;
