import { Button } from "./Button";
import "./modal.css";
import Price from "./Price";

export const Modal = ({ children, close, product }) => {
  return (
    <div className="modal-container" onClick={close}>
      <div className="modal-content" onClick={(e) => e.stopPropagation()}>
        {children}
        <div className="modal-left">
          <div className="modal-price">
            <Price item={product} />
          </div>
          <div className="modal__image-large">
            <img
              className="image-large"
              src={product.imageUrl}
              alt={product.price}
              width="50"
            />
          </div>
          <div className="modal__image-small">
            <img
              className="image-small"
              src={product.imageUrl}
              alt={product.price}
              width="50"
            />
          </div>
        </div>
        <div className="modal-right">
          <div className="modal-title">{product.title}</div>
          <div className="modal-button">
            <Button
              text="Add To My Inventory"
              onClick={(e) => {
                e.stopPropagation();
              }}
            />
          </div>
          <div className="modal__tags">
            <div className="tags-title">Product Details</div>
          </div>
          <div className="modal-description">{product.description}</div>
        </div>
        <div className="modal__exit">
          <div className="exit-button" onClick={close}>
            <span>&#10006;</span>
          </div>
        </div>
      </div>
    </div>
  );
};
