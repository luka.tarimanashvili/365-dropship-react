import { useState } from "react";

const Filter = ({ text }) => {
  const [shipFrom, setShipFrom] = useState(false);

  const filterList = () => {
    setShipFrom(!shipFrom);
  };

  return (
    <div className="owner-sort">
      <button className="owner-button" onClick={filterList}>
        <div className="owner-title">{text}</div>
        <div className={`owner-title owner-symbol ${shipFrom && "rotate"}`}>
          &#8744;
        </div>
      </button>
      <ul className={`owner-filter ${shipFrom && "visible"}`}>
        <li className="owner-list"></li>
      </ul>
    </div>
  );
};

export default Filter;
