import { Button } from "./Button";
import Price from "./Price";
import { Link, useParams } from "react-router-dom";
import { Checkbox, ThemeProvider, createMuiTheme } from "@material-ui/core";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import Api from "../Api";

const checkboxTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#4dd0e1",
    },
    secondary: {
      main: "#F3F3F3",
    },
  },
});

export const ItemCard = ({ item, toggleSelect, cardImage, inventory }) => {
  const { category } = useParams();
  const addToCart = async (e, productId) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      const cartApi = Api("api/v1/cart/add");

      await cartApi.post("", { productId, qty: 1 });
      alert("Added in cart");
    } catch (err) {
      console.log("this is err", err);
      alert("Fail to add");
    }
  };
  const removeFromCart = async (e, productId) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      const cartApi = Api(`api/v1/cart/remove/${productId}`);

      await cartApi.post("", { productId, qty: 1 });
      alert("Deleted");
    } catch (err) {
      alert("Fail to delete");
    }
  };
  return item.hidden ? null : (
    <Link
      className="item-url"
      to={
        category !== "allProducts"
          ? `/inventory/someProducts/${item.id}`
          : `/category/allProducts/${item.id}`
      }
    >
      <div className={`item ${item.selected && "highlited"}`}>
        <div className={`item-header ${item.selected && "visible"}`}>
          <ThemeProvider theme={checkboxTheme}>
            <Checkbox
              checked={item.selected || false}
              color={"primary"}
              icon={<RadioButtonUncheckedIcon color={"secondary"} />}
              checkedIcon={<CheckCircleIcon />}
              onClick={(e) => {
                e.stopPropagation();
              }}
              onChange={() => toggleSelect(item.id)}
            />
          </ThemeProvider>
          <div className="item-button">
            <Button
              text={inventory ? "Add To Inventory" : "Delete Item"}
              onClick={
                inventory
                  ? (e) => addToCart(e, item.id)
                  : (e) => removeFromCart(e, item.id)
              }
            />
          </div>
        </div>
        <div className="item__imageHolder">
          <img
            className="imageHolder-image"
            src={cardImage ? item.image : item.imageUrl}
            alt={item.price + "$"}
            width="50"
          />
        </div>
        <div className="item__details">
          <div className="details-title">{item.title.toUpperCase()}</div>
          <div className="details-category">
            <div className="details-text">Category:</div>
          </div>
          <Price item={item} />
        </div>
      </div>
    </Link>
  );
};
