import { useEffect, useState } from "react";
import CatalogItem from "../components/mainCatalog/catalog/CatalogItem";
import { Grid } from "@material-ui/core";
import axios from "axios";

const Cart = () => {
  const SERVER_URL = `http://18.185.148.165:3000/`;
  const [cartProducts, setCartProducts] = useState({});

  const getCart = async () => {
    try {
      const result = await axios.get(SERVER_URL + "api/v1/cart");
      console.log(result);
      return result.data.data;
    } catch (err) {
      if (err.response.status === 401) {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        window.location.href = "/";
      }
      console.log(err);
    }
  };

  return (
    <Grid container className="catalog">
      {cartProducts.cartItem &&
        cartProducts?.cartItem?.items.map((item) => (
          <Grid item lg={3} md={4} sm={6} xl={3} xs={12}>
            <CatalogItem
              setCartProducts={setCartProducts}
              qty={item.qty}
              key={item.id}
              id={item.id}
              price={item.price}
              title={item.title}
              image={item.image}
              description={item.description}
              val="cart"
            />
          </Grid>
        ))}
    </Grid>
  );
};
export default Cart;
