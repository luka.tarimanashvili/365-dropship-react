import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Filter from "./Filter";
import RangeSlider from "./RangeSlider";

const SidebarRight = () => {
  const [categoryOpen, setCategoryOpen] = useState(false);
  const [nicheOpen, setNicheOpen] = useState(false);

  const history = useHistory();

  const categoryList = () => {
    setTimeout(() => {
      setCategoryOpen(!categoryOpen);
    }, 100);
  };

  const nicheList = () => {
    setNicheOpen(!nicheOpen);
  };

  const textNiche = "Choose Niche";
  const textCategory = "Choose Category";

  useEffect(() => {
    const handleClick = () => setCategoryOpen(false);

    if (categoryOpen) {
      document.addEventListener("click", handleClick);
    } else {
      document.removeEventListener("click", handleClick);
    }

    return () => document.removeEventListener("click", handleClick);
  }, [categoryOpen]);

  useEffect(() => {
    const handleClick = () => setNicheOpen(false);

    if (nicheOpen) {
      document.addEventListener("click", handleClick);
    } else {
      document.removeEventListener("click", handleClick);
    }

    return () => document.removeEventListener("click", handleClick);
  }, [nicheOpen]);

  return (
    <div className="sidebar-right">
      <div className="sidebar__options">
        <div className="options-niche">
          <button className="niche-tag" onClick={nicheList}>
            <div className="options-title">{textNiche}</div>
            <div
              className={`options-title options-symbol ${
                nicheOpen && "rotate"
              }`}
            >
              &#8744;
            </div>
          </button>
          <ul className={`options-sort ${nicheOpen && "visible"}`}>
            <li className="options-list">...</li>
          </ul>
        </div>
        <div className="options-category">
          <button className="category-tag" onClick={categoryList}>
            <div className="options-title">{textCategory}</div>
            <div
              className={`options-title options-symbol ${
                categoryOpen && "rotate"
              }`}
            >
              &#8744;
            </div>
          </button>
          <ul className={`options-sort ${categoryOpen && "visible"}`}>
            <li
              className="options-list"
              onClick={() => history.push(`/category/allProducts`)}
            >
              ...
            </li>
          </ul>
        </div>
      </div>
      <div className="options__filter">
        <div className="filter__owner">
          <Filter text="Ship From" />
          <Filter text="Ship To" />
          <Filter text="Select Supplier" />
        </div>
        <div className="filter-slider">
          <RangeSlider name="PRICE RANGE" min={0} max={1000} type={"$"} />
          <RangeSlider name="PROFIT RANGE" min={0} max={100} type={"%"} />
        </div>
      </div>
    </div>
  );
};

export default SidebarRight;
