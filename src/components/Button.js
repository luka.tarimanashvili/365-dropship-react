export const Button = ({ onClick, text }) => (
  <button className="button" onClick={onClick}>
    {text}
  </button>
);
