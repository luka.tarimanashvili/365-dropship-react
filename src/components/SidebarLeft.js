import logo from "../sidebar images/logo.png";
import profile from "../sidebar images/profile.png";
import speed from "../sidebar images/speedometer.svg";
import catalog from "../sidebar images/catalog.svg";
import cube from "../sidebar images/cube.svg";
import shop from "../sidebar images/shop.svg";
import calendar from "../sidebar images/calendar.svg";
import reverse from "../sidebar images/reverse.svg";
import list from "../sidebar images/list.svg";
import { Link } from "react-router-dom";

const SidebarLeft = () => {
  return (
    <div className="sidebar__left">
      <div className="left-head">
        <div className="sidebar-image">
          <img className="sidebar-logo" src={logo} alt="logo"></img>
        </div>
      </div>
      <div className="left-main">
        <Link to="/profile">
          <div className="sidebar-image">
            <img src={profile} alt="profile"></img>
          </div>
        </Link>
        <div className="sidebar-image">
          <img src={speed} alt="speed"></img>
        </div>
        <Link to="/category/allProducts">
          <div className="sidebar-image">
            <img src={catalog} alt="catalog"></img>
          </div>
        </Link>
        <Link to="/Inventory/someProducts">
          <div className="sidebar-image">
            <img src={cube} alt="inventory"></img>
          </div>
        </Link>
        <div className="sidebar-image">
          <img src={shop} alt="shop"></img>
        </div>
        <div className="sidebar-image">
          <img src={calendar} alt="calendar"></img>
        </div>
        <div className="sidebar-image">
          <img src={reverse} alt="reverse"></img>
        </div>
        <div className="sidebar-image">
          <img src={list} alt="list"></img>
        </div>
      </div>
    </div>
  );
};

export default SidebarLeft;
