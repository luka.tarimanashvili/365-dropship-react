import SidebarLeft from "./SidebarLeft";
import SidebarRight from "./SidebarRight";

const Sidebar = ({ categorySort }) => {
  return (
    <div className="main__sidebar">
      <SidebarLeft />
      <SidebarRight categorySort={categorySort} />
    </div>
  );
};

export default Sidebar;
