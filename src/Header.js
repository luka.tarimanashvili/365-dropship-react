import { useEffect, useState } from "react";
import { useRouteMatch } from "react-router";
import { Button } from "./components/Button";
import searchButton from "./search.png";
// import axios from "axios";
import Check from "./Check.svg";

export const Header = ({ selectAll, unselectAll, total, search, select }) => {
  const [text, setText] = useState("");
  const match = useRouteMatch();
  // const SERVER_URLV1 = "http://18.185.148.165:3000/api/v1/";

  // const getCart = () => {
  //   axios.get(SERVER_URLV1 + "cart", {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     },
  //   });
  // };

  // const addCart = async (productId, qty) => {
  //   const results = await axios.post(SERVER_URLV1 + "api/v1/cart/add", {
  //     productId,
  //     qty,
  //   });
  //   return results.data.data;
  // };

  // useEffect(() => {
  //   getCart();
  // }, []);

  useEffect(() => {
    setText("");
  }, [match.params.category]);

  const handleEnter = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      search(text);
    }
  };

  return (
    <div className="header">
      <div className="header-left">
        <div className="big-button">
          <Button text="select all" onClick={selectAll} />
        </div>
        <span className="separator" />
        <span className="selected-text">
          <span className="selected-title">selected out of</span>
          <span>{total} products</span>
        </span>
        <button onClick={select} className="hide">
          <img src={Check} alt="check" />
        </button>
        <div className="clearSelected">
          <Button text="clear selected" onClick={unselectAll} />
        </div>
        <div className="clear">
          <Button text="clear" onClick={unselectAll} />
        </div>
      </div>
      <div className="header-right">
        <div className="header-nav">
          <div className="header__search">
            <input
              placeholder="search..."
              className="search-input"
              onChange={(e) => setText(e.target.value)}
              value={text}
              onBlur={() => search(text)}
              onKeyDown={handleEnter}
            />
            <button className="search-icon" onChange={() => search(text)}>
              <span>
                <img src={searchButton} alt="search"></img>
              </span>
            </button>
          </div>
          <Button text="Add To Inventory" />
        </div>
        <button className="questionMark">?</button>
      </div>
    </div>
  );
};
