import { React, useState } from "react";
import logo from "../components/loginLogo.png";
import "../components/login.css";
import axios from "axios";
import { useHistory } from "react-router-dom";

const SERVER_URL = "http://18.185.148.165:3000/";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");
  const [firstName, setFisrtName] = useState("");
  const [lastName, setLastName] = useState("");
  const [signUp, setSignUp] = useState(false);
  const history = useHistory();

  const show = () => {
    setSignUp(!signUp);
  };

  const register = () => {
    axios.post(SERVER_URL + "register", {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
    });
  };

  const login = () => {
    axios
      .post(SERVER_URL + "login", {
        email,
        password,
      })
      .then((user) => {
        localStorage.setItem("user", user.data);
        localStorage.setItem("token", user.data.data.token);
        history.push("/category/allProducts");
      })
      .catch((err) => {
        alert("Log in failed");
        console.log(err);
      });
  };

  return (
    <div className="login">
      <div className="login-input">
        <div className="login-header">
          <div className="login__logo">
            <img className="logo-image" src={logo} alt="logo"></img>
          </div>
          {!signUp && <div className="login-title">Memberes Log In</div>}
          {signUp && <div className="login-title">Sign Up</div>}
        </div>
        <div className="login__authorization">
          {signUp && (
            <input
              className="authorization-input"
              placeholder="First Name"
              type="text"
              name="fitsName"
              value={firstName}
              onChange={(e) => setFisrtName(e.target.value)}
            />
          )}
          {signUp && (
            <input
              className="authorization-input"
              placeholder="Last Name"
              type="text"
              name="lastName"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          )}
          <input
            className="authorization-input"
            placeholder="E-mail"
            type="text"
            name="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <input
            className="authorization-input"
            placeholder="Password"
            type="text"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          {signUp && (
            <input
              className="authorization-input"
              placeholder="Confirm Password"
              type="text"
              name="Password Confirmation"
              value={passwordConfirmation}
              onChange={(e) => setPasswordConfirmation(e.target.value)}
            />
          )}
        </div>
        {!signUp && <div className="forgotPassword">Forgot password ?</div>}
        {!signUp && (
          <input
            className="submit"
            type="submit"
            value="Log In"
            onClick={login}
          />
        )}
        {signUp && (
          <input
            className="submit"
            type="submit"
            value="Sign Up"
            onClick={register}
          />
        )}
        {!signUp && <div>Dont have an account?</div>}
        {signUp && <div>Have an account?</div>}
        {!signUp && (
          <button className="submit" onClick={show}>
            Sign Up
          </button>
        )}
        {signUp && (
          <button className="submit" onClick={show}>
            Sign In
          </button>
        )}
      </div>
    </div>
  );
}
