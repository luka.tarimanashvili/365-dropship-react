import SidebarLeft from "../components/SidebarLeft";
import "../components/profile.css";

export default function Profile() {
  const token = localStorage.getItem("token");

  const logOut = () => {
    localStorage.removeItem("token");
    window.location.href = "/";
  };

  if (token) {
    return (
      <div className="Catalog">
        <main className="main">
          <SidebarLeft />
          <div className="profile">
            <div className="profile-header">
              <button onClick={logOut} className="logOut">
                Log Out bb
              </button>
            </div>
          </div>
        </main>
      </div>
    );
  } else {
    window.location.href = "/";
  }
}
