import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  DialogActions,
  Button,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

const NewModal = (isOpen) => {
  const [open, setOpen] = useState(false);
  const history = useHistory();

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  const openDialog = () => {
    setOpen(true);
  };

  const closeDialog = () => {
    history.push("/category");
    setOpen(false);
  };
  return (
    <Dialog onClose={closeDialog} open={open}>
      <DialogTitle>Some Text</DialogTitle>
      <DialogContent>
        <Typography gutterBottom>
          Lgbasdashfagbjdsbgadsbgasbndgbkvnl vaKA DJOI
          AJDSFASHFOIAhfhdfhOHJBHJGSDJhoh o dhsaifndsjb
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} color="primary" variant="outlined">
          close
        </Button>
        <Button color="primary" variant="contained">
          save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default NewModal;
