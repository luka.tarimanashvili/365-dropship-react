import { useEffect, useState } from "react";
import { Header } from "../Header";
import { Sort } from "../Sort";
import { ItemsList } from "../ItemsList";
import "../Catalog.css";
import Api from "../Api";
import { Modal } from "../components/Modal.js";
import Sidebar from "../components/Sidebar";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";

function Catalog() {
  // const [searchText, setSearchText] = useState([]);
  const [products, setProducts] = useState([]);
  const [product, setProduct] = useState(null);

  const dispatch = useDispatch();
  const match = useRouteMatch();
  const history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const productApi = Api("api/v1/products");
    if (token) {
      productApi
        .get()
        .then((res) => {
          localStorage.setItem("products", JSON.stringify(res.data.data));
          dispatch({
            type: "Products_Arrived",
            payload: res.data.data,
          });
          // setSearchText(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      window.location.href = "/";
    }
  });

  useEffect(() => {
    if (match.params.id) {
      const modalProduct = JSON.parse(localStorage.getItem("products")).find(
        (item) => item.id === +match.params.id
      );
      setProduct(modalProduct);
    }
  }, [match.params.id]);

  const selectAll = () => {
    setProducts((items) =>
      items.map((item) => {
        if (!item.hidden) {
          item.selected = true;
        }
        return item;
      })
    );
  };

  const toggleSelect = (id) => {
    setProducts((items) =>
      items.map((item) => {
        if (item.id === id) {
          return Object.assign({}, item, { selected: !item.selected });
        }
        return item;
      })
    );
  };

  const unselectAll = () => {
    setProducts((items) =>
      items.map((item) => {
        if (!item.hidden) {
          item.selected = false;
        }
        return item;
      })
    );
  };

  const select = () => {
    setProducts((items) => {
      return items.map((item) => {
        let newItem;
        if (!item.hidden && !item.selected > item.selected) {
          newItem = { ...item, selected: true };
        } else {
          newItem = { ...item, selected: false };
        }
        return newItem;
      });
    });
  };

  // const search = (text) => {
  //   const term = text.trim().toLowerCase();
  //   const searched = store.filter((item) =>
  //     item.title.toLowerCase().includes(term)
  //   );
  //   // setProducts(searched);
  //   dispatch({
  //     type: "Products_Arrived",
  //     payload: searched,
  //   });
  // };

  return (
    <div className="Catalog">
      <main className="main">
        <Sidebar />
        <div className="main__container">
          <Header
            select={select}
            selectAll={selectAll}
            unselectAll={unselectAll}
            total={products.filter((item) => !item.hidden).length}
            // search={search}
          />
          <Sort />
          <div className="container__catalog">
            <ItemsList toggleSelect={toggleSelect} inventory />
          </div>
        </div>
      </main>
      {product && (
        <Modal
          close={() => {
            setProduct(null);
            history.goBack();
          }}
          product={product}
        />
      )}
    </div>
  );
}

export default Catalog;
