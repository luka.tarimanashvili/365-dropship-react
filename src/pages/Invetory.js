import React from "react";
import Sidebar from "../components/Sidebar";
import { Header } from "../Header";
import { useEffect, useState } from "react";
import { ItemsList } from "../ItemsList";
import Api from "../Api";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Modal } from "../components/Modal";
import { useDispatch } from "react-redux";

export default function Inventory() {
  const [product, setProduct] = useState();

  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  useEffect(() => {
    const cartApi = Api("api/v1/cart");
    cartApi
      .get()
      .then((res) => {
        localStorage.setItem(
          "cart",
          JSON.stringify(res.data.data.cartItem.items)
        );
        dispatch({
          type: "Cart_Arrived",
          payload: res.data.data.cartItem.items,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
  useEffect(() => {
    if (match.params.id) {
      const modalProduct = JSON.parse(localStorage.getItem("products")).find(
        (item) => item.id === +match.params.id
      );
      setProduct(modalProduct);
    }
  }, [match.params.id]);

  return (
    <div className="Catalog">
      <main className="main">
        <Sidebar />
        <div className="main__container">
          <Header />
          <div className="container-cardList">
            <ItemsList cardImage />
          </div>
          {product && (
            <Modal
              close={() => {
                setProduct(null);
                history.goBack();
              }}
              product={product}
            />
          )}
        </div>
      </main>
    </div>
  );
}
