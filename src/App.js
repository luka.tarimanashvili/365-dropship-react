import CatalogPage from "./pages/CatalogPage";
import { Route, Redirect, Switch } from "react-router-dom";
import Dashbord from "./pages/Dashbord";
import Login from "./pages/Login";
import Inventory from "./pages/Invetory";
import Profile from "./pages/Profile";

function App() {
  const token = localStorage.getItem("token");
  return (
    <>
      <Switch>
        <Route exact path="/">
          <Redirect to={token ? "/category/allProducts" : "/login"} />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/category/:category?/:id?">
          <CatalogPage />
        </Route>
        <Route exact path="/dashboard">
          <Dashbord />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/inventory/:category?/:id?">
          <Inventory />
        </Route>
      </Switch>
    </>
  );
}

export default App;
